import { useContext } from "react";
import "./Cart.css";

import CartContext from "../context/cart/CartContext";
import CartItem from "./CartItems";
function Cart() {
  const { showCart, showHideCart, cartItems } = useContext(CartContext);
  return (
    <>
      {showCart && (
        <div className="cart__wrapper">
          <div style={{ textAlign: "right" }}>
            <i style={{ cursor: "pointer" }} onClick={showHideCart}></i>
          </div>
          <div>
            {cartItems.length === 0 ? (
              <h3>The Cart is empty</h3>
            ) : (
              <ul>
                {cartItems.map((item) => (
                  <CartItem key={cartItems.id} item={item} />
                ))}
              </ul>
            )}
          </div>
          <div className="cart__cartTotal">
            <div>Cart Total</div>
            <div style={{ marginLeft: 5 }}>
              {cartItems.reduce((amount, item) => item.price + amount, 0)}
            </div>
          </div>
        </div>
      )}
    </>
  );
}

export default Cart;
