import { useContext } from "react";
import "./ProductCard.css";

import CartContext from "../context/cart/CartContext";

const ProductCard = ({ product }) => {
  const { addToCart } = useContext(CartContext);

  return (
    <div className="productCard-wrapper">
      <div>
        <img className="productCard-img" src={product.image} alt="" />
        <h4>{product.name}</h4>
        <div className="ProductCard-price">
          <h5>{product.price}}</h5>
        </div>

        <button
          className="ProductCard-button"
          onClick={() => addToCart(product)}
        >
          Add to basket
        </button>
      </div>
    </div>
  );
};

export default ProductCard;
