import React from "react";
import "./App.css";
import Cart from "./components/Cart";
import Nav from "./components/Nav";
import HomeScreen from "./pages/HomeScreen";

function App() {
  return (
    <div className="App">
      <Nav />
      <Cart />
      <HomeScreen />
    </div>
  );
}

export default App;
