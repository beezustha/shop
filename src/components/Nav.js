import React, { useContext } from "react";
import CardContext from "../context/cart/CartContext";
import "./Nav.css";
const Nav = () => {
  const { cartItems, showHideCart } = useContext(CardContext);
  return (
    <nav>
      <div className="nav__left">Store</div>
      <div className="nav__middle">
        <div className="input__wrapper">
          <input type="text" />
          <i className="fas fa-search" />
        </div>
      </div>
      <div className="nav__right">
        <div className="cart__icon">
          <i
            onClick={showHideCart}
            className="fas fa-shopping-cart"
            aria-hidden="true"
          />
          {cartItems.length > 0 && (
            <div className="items__count">
              <span>{cartItems.length}</span>
              {/* cartItems >0 then number will pop up beside cart icon */}
            </div>
          )}
        </div>
      </div>
    </nav>
  );
};

export default Nav;
