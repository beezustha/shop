import React, { useContext } from "react";
import CartContext from "../context/cart/CartContext";
import "./CartItem.css";
function CartItems({ item }) {
  const { removeItem } = useContext(CartContext);
  return (
    <div>
      <li className="classItem__item">
        <img src={item.image} alt="" srcset="" />
        <div>{item.name}</div>
        <button onClick={() => removeItem(item._id)}>Remove</button>
      </li>
    </div>
  );
}

export default CartItems;
